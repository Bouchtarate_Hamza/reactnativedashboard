# React Native Dashboard App

Salam 3alaykom and welcome to the React native Dashboard App Test! This application provides a customizable dashboard layout for displaying key information and statistics.

## Prerequisites

- Node.js and npm installed
- React Native dev env and set up Expo / Expo go app to see the app 

### Installation

1. Clone the repository: git clone https://gitlab.com/Bouchtarate_Hamza/reactnativedashboard.git

2. cd reactnativedashboard

3. npm install

#### Running the App

1. Download the Expo Go application in Andoid or Iphone IOS

2. npx expo start