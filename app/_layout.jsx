import "react-native-gesture-handler";
import { Drawer } from "expo-router/drawer";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { Stack } from "expo-router";

export default function Layout() {
  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <Drawer>
        <Drawer.Screen
          name="index"
          options={{
            drawerActiveTintColor: "#f5675b",
            drawerLabel: "Login/Register",
            headerTitle: "Login/Register",
          }}
        />
        <Drawer.Screen
          name="dashboard"
          options={{
            drawerActiveTintColor: "#f5675b",
            drawerLabel: "Dashboard",
            headerTitle: "Dashboard",
          }}
        />
        <Drawer.Screen
          name="List"
          options={{
            drawerActiveTintColor: "#f5675b",
            drawerLabel: "List",
            headerTitle: "List",
          }}
        />
        <Drawer.Screen
          name="Profile"
          options={{
            drawerActiveTintColor: "#f5675b",
            drawerLabel: "Profile",
            headerTitle: "Profile",
          }}
        />
        <Drawer.Screen
          name="Logout"
          options={{
            drawerActiveTintColor: "#f5675b",
            drawerLabel: "Logout",
            headerTitle: "Logout",
          }}
        />
      </Drawer>
    </GestureHandlerRootView>
  );
}
