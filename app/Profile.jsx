import { View, Text } from "react-native";
import * as React from "react";

export default function Dashboard() {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ fontSize: 36, fontWeight: "900" }}>Profile</Text>
    </View>
  );
}
