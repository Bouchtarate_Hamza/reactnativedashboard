import { useState } from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";
import { Link } from "expo-router";

export default function index() {
  const [haveAcc, setHaveAcc] = useState(false);
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Text
        style={{
          fontWeight: "bold",
          fontSize: 24,
          color: "#f5675b",
        }}
      >
        Welcome
      </Text>
      <View>
        <TextInput
          style={{
            marginTop: 16,
            backgroundColor: "#eee",
            borderRadius: 15,
            padding: 8,
            width: 300,
            paddingLeft: 16,
            fontSize: 16,
            display: haveAcc ? "none" : "flex",
          }}
          placeholder="username"
        />
        <View>
          <TextInput
            style={{
              paddingLeft: 16,
              marginTop: 16,
              backgroundColor: "#eee",
              borderRadius: 15,
              padding: 8,
              width: 300,
              fontSize: 16,
              display: haveAcc ? "none" : "flex",
            }}
            placeholder="e-mail"
          />
        </View>
      </View>
      <View>
        <TextInput
          style={{
            paddingLeft: 16,
            marginTop: 16,
            backgroundColor: "#eee",
            borderRadius: 15,
            padding: 8,
            width: 300,
            fontSize: 16,
          }}
          placeholder="username/e-mail"
        />
      </View>
      <View>
        <TextInput
          style={{
            paddingLeft: 16,
            marginTop: 16,
            backgroundColor: "#eee",
            borderRadius: 15,
            padding: 8,
            width: 300,
            fontSize: 16,
          }}
          secureTextEntry={true}
          placeholder="password"
        />
      </View>
      <View>
        <TextInput
          secureTextEntry={true}
          style={{
            paddingLeft: 16,
            marginTop: 16,
            backgroundColor: "#eee",
            borderRadius: 15,
            padding: 8,
            width: 300,
            fontSize: 16,
            display: haveAcc ? "none" : "flex",
          }}
          placeholder="confirm password"
        />
      </View>
      {haveAcc ? (
        <View>
          <Text style={{ fontSize: 16, marginTop: 12 }}>
            New user{" "}
            <Text
              style={{ color: "#f5675b" }}
              onPress={() => setHaveAcc(false)}
            >
              SignUp
            </Text>
          </Text>
        </View>
      ) : (
        <View>
          <Text style={{ fontSize: 16, marginTop: 12 }}>
            Already have account{" "}
            <Text style={{ color: "#f5675b" }} onPress={() => setHaveAcc(true)}>
              Login
            </Text>
          </Text>
        </View>
      )}
      <TouchableOpacity>
        <Link
          href="/dashboard"
          style={{
            marginTop: 16,
            backgroundColor: "#f5675b",
            paddingHorizontal: 32,
            paddingVertical: 12,
            borderRadius: 8,
            fontSize: 20,
            color: "#fff",
          }}
        >
          Submit
        </Link>
      </TouchableOpacity>
    </View>
  );
}
