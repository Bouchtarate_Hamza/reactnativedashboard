// import { View, Text } from "react-native";
// import * as React from "react";

// export default function List() {
//   return (
//     <View>
//       <Text>Users List</Text>
//     </View>
//   );
// }
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import Data from "../components/Data";
const List = () => {
  const [active, setActive] = useState("Data");
  return (
    <SafeAreaView style={styles.container}>
      {active === "Data" && <Data />}
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tab} onPress={() => setActive("Data")}>
          <Text style={styles.tabText}>Data</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "lightcyan",
  },
  tabBar: {
    flexDirection: "row",
    borderTopColor: "#333333",
    borderTopWidth: 1,
  },
  tab: {
    height: 50,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  tabText: {
    color: "darkblue",
    fontSize: 20,
    fontWeight: "bold",
  },
});
export default List;
